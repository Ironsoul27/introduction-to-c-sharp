﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace CharacterCreator
{
    class Character
    {
        private string name;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        private SpriteSheet spritesheet;

        public SpriteSheet SpriteSheet
        {
            get { return spritesheet; }
        }

        private List<Layer> layers = new List<Layer>();

        public Point tileCoordinates = new Point(0, 0);

        public Character(string name, SpriteSheet spritesheet)
        {
            this.name = name;
            this.spritesheet = spritesheet;
        }

        public void AddLayer(Layer layer)
        {
            layers.Add(layer);
        }

        public override string ToString()
        {
            return base.ToString() + "\n\tpath: \t" + spritesheet.ToString() +
                                "\n\ttile coordinates: \t" + layers[0].TileCoordinates.ToString();
        }
    }
}
