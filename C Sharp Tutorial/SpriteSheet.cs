﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace CharacterCreator
{
    class SpriteSheet
    {
        private Image image = null;

        public string path;

        public int GetWidth
        {
            get
            {
                return (image != null) ? image.Width : 0;
            }
        }

        public int GetHeight
        {
            get
            {
                return (image != null) ? image.Height : 0;
            }
        }

        public SpriteSheet(string path)
        {
            this.path = path;
            Load();
        }

        public void Load()
        {
            image = Image.FromFile(path);
        }
    }
}
